import java.util.Arrays;
import java.util.Scanner;

class Solution {
    public int findDuplicate(int[] nums) {
        int slow = nums[0];
        int fast = nums[0];

        // Move slow one step and fast two steps until they meet
        do {
            slow = nums[slow];
            fast = nums[nums[fast]];
        } while (slow != fast);

        // Find the entrance to the cycle
        slow = nums[0];
        while (slow != fast) {
            slow = nums[slow];
            fast = nums[fast];
        }

        return slow;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the length of the array: ");
        int n = scanner.nextInt();
        int[] nums = new int[n + 1];

        System.out.println("Enter the elements of the array:");
        for (int i = 0; i < n + 1; i++) {
            nums[i] = scanner.nextInt();
        }

        int duplicate = solution.findDuplicate(nums);
        System.out.println("The duplicate number is: " + duplicate);
    }
}
